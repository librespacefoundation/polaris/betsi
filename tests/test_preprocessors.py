"""Module for testing preprocessors
"""

import numpy as np
import pandas as pd
import pytest
from sklearn import preprocessing

from betsi import preprocessors


def test_happy_normalize_all_data():
    """Test happy cases for normalize_all_data
    """
    test_df = pd.DataFrame.from_records([[1, -1, 2], [2, 0, 0], [0, 1, -1]])
    test_arr = test_df.to_numpy()

    # Testing MinMaxScaler and custom scaler process
    n_test_df, df_scaler = preprocessors.normalize_all_data(
        test_df, preprocessing.MinMaxScaler())
    n_test_arr, arr_scaler = preprocessors.normalize_all_data(
        test_arr, preprocessing.MinMaxScaler())

    assert isinstance(df_scaler, preprocessing.MinMaxScaler)
    assert isinstance(arr_scaler, preprocessing.MinMaxScaler)
    assert isinstance(n_test_df, pd.DataFrame)
    assert isinstance(n_test_arr, np.ndarray)
    assert n_test_arr[0][1] == 0.0  # -1 transformed to 0
    assert n_test_arr[0][2] == 1.0  # 2 transformed to 1
    assert np.array_equal(n_test_arr, n_test_df.values)

    # Testing StandardScaler as default scaler
    n_test_df2, df_scaler2 = preprocessors.normalize_all_data(test_df)
    n_test_arr2, arr_scaler2 = preprocessors.normalize_all_data(test_arr)

    assert isinstance(df_scaler2, preprocessing.StandardScaler)
    assert isinstance(arr_scaler2, preprocessing.StandardScaler)
    assert isinstance(n_test_df2, pd.DataFrame)
    assert isinstance(n_test_arr2, np.ndarray)
    assert np.array_equal(n_test_arr2, n_test_df2.values)


def test_sad_normalize_all_data():
    """Test sad cases for normalize_all_data
    """

    # Test wrong data input type
    test_l = [[1, -1, 2], [2, 0, 0], [0, 1, -1]]
    with pytest.raises(TypeError):
        _ = preprocessors.normalize_all_data(test_l)

    # Test wrong scaler input type
    def bad_scaler(x_data=None):
        """ Does nothing """
        return x_data

    test_df = pd.DataFrame.from_records([[1, -1, 2], [2, 0, 0], [0, 1, -1]])
    with pytest.raises(TypeError):
        _ = preprocessors.normalize_all_data(test_df, bad_scaler())


@pytest.mark.parametrize("window_size, stride", [(2, -2), (2, 1)])
def test_happy_convert_to_from_column(window_size, stride):
    """Test happy case for convert_to_column
    """
    test_arr = pd.DataFrame(np.array([range(10), range(11, 21)]))

    column_df_1 = preprocessors.convert_to_column(test_arr, window_size,
                                                  stride)
    _ = preprocessors.convert_from_column(column_df_1, window_size, stride)


@pytest.mark.parametrize("window_size, stride", [(0, 2), (2, 0)])
def test_sad_convert_to_column(window_size, stride):
    """Test sad case for convert_to_column
    """
    test_arr = np.array([range(10), range(11, 21)])

    with pytest.raises(ValueError):
        _ = preprocessors.convert_to_column(test_arr, window_size, stride)
